# README #

Projeto de exemplo para o LAB01 da disciplina INF321 - UNICAMP.

### Instruções ###
Siga as instruções do LAB01 disponibilizadas no Moodle e o guia de configuração do ambiente.

* [INF 321 - LAB01 - Guia de Instalação e Configuração do Ambiente](https://docs.google.com/document/d/e/2PACX-1vSSn5iszgAsfNDtYxfXxrWZnVoYy0fTXp6QJz8uFoFUGicvbFu4TNFT3SiUs0FaiF89OZL6n5sJdEYv/pub)
* [Instruções LAB01](https://moodle.lab.ic.unicamp.br/moodle/pluginfile.php/7469/mod_folder/content/0/INF321_LAB01_Orientacoes.pdf?forcedownload=1)

