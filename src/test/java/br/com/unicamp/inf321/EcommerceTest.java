package br.com.unicamp.inf321;

import br.com.unicamp.inf321.helper.GraphWalkerTestBuilder;
import br.com.unicamp.inf321.models.EcommerceModel;
import br.com.unicamp.inf321.observers.GraphStreamObserver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.commons.io.FileUtils;
import org.assertj.core.api.Assertions;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.stream.file.FileSinkImages;
import org.graphstream.stream.file.FileSinkImages.LayoutPolicy;
import org.graphstream.stream.file.FileSinkImages.OutputType;
import org.graphstream.stream.file.FileSinkImages.RendererType;
import org.graphstream.stream.file.FileSinkImages.Resolutions;
import org.graphstream.ui.view.Viewer;
import org.graphwalker.core.condition.EdgeCoverage;
import org.graphwalker.core.condition.ReachedEdge;
import org.graphwalker.core.condition.ReachedVertex;
import org.graphwalker.core.condition.TimeDuration;
import org.graphwalker.core.event.Observer;
import org.graphwalker.core.generator.AStarPath;
import org.graphwalker.core.generator.CombinedPath;
import org.graphwalker.core.generator.QuickRandomPath;
import org.graphwalker.core.generator.RandomPath;
import org.graphwalker.java.test.Result;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class EcommerceTest {
    public final static Path MODEL_PATH =
            Paths.get("/br/com/unicamp/inf321/Ecommerce.graphml");
    private static AndroidDriver<MobileElement> driver;
    @Rule
    public TestName testName = new TestName();
    private Observer observer;
    private Graph graph;
    private Viewer viewer;

    @BeforeClass
    public static void setup() {
        // seta os capabilities do android driver
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Samsung Galaxy S6");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.0");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(MobileCapabilityType.APP, "/root/tmp/app-extensao-debug.apk");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE,
                "br.com.sofist.ecommerce.ext");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY,
                "br.com.sofist.ecommerce.SplashActivity");
        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "120");
        capabilities.setCapability(AndroidMobileCapabilityType.UNICODE_KEYBOARD, "true");
        capabilities.setCapability(MobileCapabilityType.TAKES_SCREENSHOT, "true");
        capabilities.setCapability(MobileCapabilityType.CLEAR_SYSTEM_FILES, "false");
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, "false");
        capabilities.setCapability(MobileCapabilityType.NO_RESET, "false");
        capabilities.setCapability("autoLaunch", "false");
        try {
            driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.rotate(ScreenOrientation.PORTRAIT); // rotaciona tela
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

    @Before
    public void beforeTest() {
        driver.closeApp();
        // reseta aplicativo antes de cada teste
        driver.resetApp();
        // cria observer para habilitar execução do modelo animado
        System.setProperty("org.graphstream.ui.renderer",
                "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
        graph =
                new MultiGraph(EcommerceTest.class.getSimpleName() + "_" + testName.getMethodName());
        viewer = graph.display(true);
        // Let the layout work ...
        observer = new GraphStreamObserver(graph);
    }

    @After
    public void afterTest() throws Exception {
        // Capture screenshot
        File scrFile = driver.getScreenshotAs(org.openqa.selenium.OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("./screenshots/Android_"
                + EcommerceTest.class.getSimpleName() + "_" + testName.getMethodName() + ".jpg"));
        // gera screenshot do modelo animado
        FileSinkImages pic = new FileSinkImages(OutputType.JPG, Resolutions.HD720);
        pic.setLayoutPolicy(LayoutPolicy.COMPUTED_FULLY_AT_NEW_IMAGE);
        pic.setRenderer(RendererType.SCALA);
        pic.stabilizeLayout(1);
        pic.setAutofit(true);
        pic.writeAll(graph, "screenshots/Graph_" + EcommerceTest.class.getSimpleName() + "_"
                + testName.getMethodName() + ".jpg");
    }

    @Test
    public void runSmokeTest() {
        CombinedPath cp = new CombinedPath();
        cp.addPathGenerator(new AStarPath(new ReachedEdge("e_PreencherDadosCadastro")));
        cp.addPathGenerator(new AStarPath(new ReachedEdge("e_AcionarCheckBoxTermos")));
        cp.addPathGenerator(new AStarPath(new ReachedVertex("v_Home")));
        cp.addPathGenerator(new AStarPath(new ReachedEdge("e_ScrollListaProdutos")));

        Result result = new GraphWalkerTestBuilder()
                .addModel(MODEL_PATH, cp, new EcommerceModel(driver)).addObserver(observer)
                .execute(false);
        Assertions.assertThat(result.hasErrors()).isFalse();
    }

    @Test
    public void runStabilityTest() {
        Result result = new GraphWalkerTestBuilder()
                .addModel(MODEL_PATH, new RandomPath(new TimeDuration(2, TimeUnit.MINUTES)), "e_Iniciar",
                        new EcommerceModel(driver))
                .addObserver(observer) // adicona observer para ver execução do modelo animada
                .execute(false);
        Assertions.assertThat(result.hasErrors()).isFalse();
    }

    @Test
    public void runFunctionalTest() {
        Result result = new GraphWalkerTestBuilder()
                .addModel(MODEL_PATH, new RandomPath(new EdgeCoverage(100)), "e_Iniciar",
                        new EcommerceModel(driver))
                .addObserver(observer) // adicona observer para ver execução do modelo animada
                .execute(false);
        Assertions.assertThat(result.hasErrors()).isFalse();
    }

    @Test
    @Ignore("just to debug")
    public void runExemploSemModelo() throws Exception {
        EcommerceModel model = new EcommerceModel(driver);
        PageFactory.initElements(new AppiumFieldDecorator(driver, 20, TimeUnit.SECONDS), model.getRegisterObjects());
        model.getRegisterObjects().getEditTextName().setValue("Fulano de tal");
        model.getRegisterObjects().getEditTextEmail().setValue("teste@teste.com");
        model.getRegisterObjects().getEditTextPhone().setValue("19999999999");

    }

}