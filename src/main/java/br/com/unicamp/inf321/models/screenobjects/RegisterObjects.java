package br.com.unicamp.inf321.models.screenobjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

import java.util.List;

public class RegisterObjects {

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/login_presentation_line1")
    private MobileElement title;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/textinput_error")
    private List<MobileElement> fieldErrors;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/etName")
    private MobileElement editTextName;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/etEmail")
    private MobileElement editTextEmail;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/etPhone")
    private MobileElement editTextPhone;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/cbTermos")
    private MobileElement checkBoxTerms;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/tvTermos")
    private MobileElement linkPrivacyTerms;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/sign_up_button")
    private MobileElement buttonContinue;

    public MobileElement getTitle() {
        return title;
    }

    public MobileElement getEditTextName() {
        return editTextName;
    }

    public MobileElement getEditTextEmail() {
        return editTextEmail;
    }

    public MobileElement getEditTextPhone() {
        return editTextPhone;
    }

    public MobileElement getCheckBoxTerms() {
        return checkBoxTerms;
    }

    public MobileElement getButtonContinue() {
        return buttonContinue;
    }

    public MobileElement getLinkPrivacyTerms() {
        return linkPrivacyTerms;
    }

    public List<MobileElement> getFieldErrors() {
        return fieldErrors;
    }
}
