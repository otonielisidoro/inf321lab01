package br.com.unicamp.inf321.models;

import br.com.unicamp.inf321.Ecommerce;
import br.com.unicamp.inf321.helper.TestHelpers;
import br.com.unicamp.inf321.models.screenobjects.HomeObjects;
import br.com.unicamp.inf321.models.screenobjects.PrivacyTermsObjects;
import br.com.unicamp.inf321.models.screenobjects.ProductListObjects;
import br.com.unicamp.inf321.models.screenobjects.RegisterObjects;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.graphwalker.java.annotation.GraphWalker;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static org.assertj.core.api.StrictAssertions.assertThat;

@GraphWalker(value = "random(edge_coverage(100))", start = "e_Iniciar")
public class EcommerceModel extends TestHelpers implements Ecommerce {
    private RegisterObjects registerObjects;
    private HomeObjects homeObjects;
    private ProductListObjects productListObjects;
    private PrivacyTermsObjects privacyTermsObjects;
    Set<String> productListElements = new LinkedHashSet<>();

    public EcommerceModel(AndroidDriver<MobileElement> driver) {
        super(driver);
        this.registerObjects = new RegisterObjects();
        this.homeObjects = new HomeObjects();
        this.productListObjects = new ProductListObjects();
        this.privacyTermsObjects = new PrivacyTermsObjects();
    }

    @Override
    public void e_Iniciar() {
        getDriver().launchApp();
    }

    @Override
    public void e_RejeitarTermos() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                privacyTermsObjects);
        swipeVerticalUntilElementIsDisplayed(By.id(PrivacyTermsObjects.ID_BUTTON_DONT_ACCEPT), false, false);
        if (privacyTermsObjects.getButtonDontAccept().isDisplayed()) {
            privacyTermsObjects.getButtonDontAccept().click();
        }
    }

    @Override
    public void v_TermosPolitica() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                privacyTermsObjects);
        assertThat(privacyTermsObjects.getTitle().getText()).isEqualToIgnoringCase("Regulamento");
    }

    @Override
    public void e_ScrollListaProdutos() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                homeObjects);
        List<String> itensToAdd = getHomeObjects().getProductListWidget().getProducts().stream()
                .map(RemoteWebElement::getText).collect(Collectors.toList());
        if (itensToAdd.size() > 0) {
            String lastElementText = (String) itensToAdd.toArray()[itensToAdd.size() - 1];
            if (productListElements.size() > 0 && Objects.equals(lastElementText, productListElements.toArray()[productListElements.size() - 1])) {
                setAttribute("fimDaTela", true);
            } else {
                productListElements.addAll(itensToAdd);
                swipeVertical(false);
            }
        }
    }

    @Override
    public void v_Home() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                homeObjects);
        assertThat(homeObjects.getButtonSearch().isDisplayed()).isTrue();
        assertThat(homeObjects.getProductListWidget().getProducts()).asList().isNotEmpty();
    }

    @Override
    public void e_AceitarTermos() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                privacyTermsObjects);
        swipeVerticalUntilElementIsDisplayed(By.id(PrivacyTermsObjects.ID_BUTTON_ACCEPT), false, false);
        if (privacyTermsObjects.getButtonAccept().isDisplayed()) {
            privacyTermsObjects.getButtonAccept().click();
            productListElements.clear();
        }
    }

    @Override
    public void e_Continuar() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                registerObjects);
        registerObjects.getButtonContinue().click();
        productListElements.clear();
    }

    @Override
    public void e_AcionarCheckBoxTermos() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                registerObjects);
        registerObjects.getCheckBoxTerms().click();
    }

    @Override
    public void e_VisualizarTermos() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                registerObjects);
        registerObjects.getLinkPrivacyTerms().click();
    }

    @Override
    public void e_Voltar() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                privacyTermsObjects);
        privacyTermsObjects.getBackButton().click();
    }

    @Override
    public void e_logout() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                homeObjects);
        homeObjects.getOpenMenu().click();
        homeObjects.getMenuWidget().getLogoutMenu().click();
    }

    @Override
    public void e_PreencherDadosCadastro() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                registerObjects);
        registerObjects.getEditTextName().setValue((String) getAttribute("nome"));
        registerObjects.getEditTextEmail().setValue((String) getAttribute("email"));
        registerObjects.getEditTextPhone().setValue((String) getAttribute("telefone"));

    }

    @Override
    public void v_Cadastro() {
        PageFactory.initElements(new AppiumFieldDecorator(getDriver(), IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS),
                registerObjects);
        assertThat(registerObjects.getTitle().getText())
                .isEqualToIgnoringCase("Faça seu cadastro antes de começar as suas compras");
        if (getAttribute("erro").equals(Boolean.TRUE)) {
            if (getAttribute("nome") != null && ((String) getAttribute("nome")).isEmpty()) {
                assertThat(registerObjects.getFieldErrors()).asList().extracting("text").contains("Digite também o seu sobrenome");
            }
            if (getAttribute("email") != null && ((String) getAttribute("email")).isEmpty()) {
                assertThat(registerObjects.getFieldErrors()).asList().extracting("text").contains("Digite seu email");
            }
            if (getAttribute("telefone") != null && ((String) getAttribute("telefone")).isEmpty()) {
                assertThat(registerObjects.getFieldErrors()).asList().extracting("text").contains("Digite seu telefone");
            }
        }
    }

    public RegisterObjects getRegisterObjects() {
        return registerObjects;
    }

    public HomeObjects getHomeObjects() {
        return homeObjects;
    }

    public ProductListObjects getProductListObjects() {
        return productListObjects;
    }

    public PrivacyTermsObjects getPrivacyTermsObjects() {
        return privacyTermsObjects;
    }
}
