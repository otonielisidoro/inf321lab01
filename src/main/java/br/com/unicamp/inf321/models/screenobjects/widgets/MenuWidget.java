package br.com.unicamp.inf321.models.screenobjects.widgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.By;

/**
 * @author Toni Isidoro (toni.isidoro@sofist.com.br)
 */
@AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/design_navigation_view")
public class MenuWidget extends Widget {
    protected MenuWidget(MobileElement element) {
        super(element);
    }

    @AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text=\"Sair\"]")
    private MobileElement logoutMenu;

    public MobileElement getCategoryByName(String category) {
        return getWrappedDriver().findElement(By.xpath(".//android.widget.CheckedTextView[@text='" + category + "']"));
    }

    public MobileElement getLogoutMenu() {
        return logoutMenu;
    }
}
