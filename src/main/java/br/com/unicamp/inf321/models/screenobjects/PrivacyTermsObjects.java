package br.com.unicamp.inf321.models.screenobjects;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PrivacyTermsObjects {

    public static final String ID_BUTTON_ACCEPT = "br.com.sofist.ecommerce.ext:id/btAceito";
    public static final String ID_BUTTON_DONT_ACCEPT = "br.com.sofist.ecommerce.ext:id/btNaoAceito";

    @AndroidFindBy(xpath = "//android.widget.TextView[@text=\"Regulamento\"]")
    private MobileElement title;

    @AndroidFindAll({
            @AndroidBy(xpath = "//android.view.ViewGroup[@resource-id=\"br.com.sofist.ecommerce.ext:id/toolbar\"]/android.widget.ImageButton[1]"),
            @AndroidBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']", priority = 1)
    })
    private MobileElement backButton;

    @AndroidFindBy(id = ID_BUTTON_ACCEPT)
    private MobileElement buttonAccept;

    @AndroidFindBy(id = ID_BUTTON_DONT_ACCEPT)
    private MobileElement buttonDontAccept;


    public MobileElement getTitle() {
        return title;
    }

    public MobileElement getBackButton() {
        return backButton;
    }

    public MobileElement getButtonAccept() {
        return buttonAccept;
    }

    public MobileElement getButtonDontAccept() {
        return buttonDontAccept;
    }
}
