package br.com.unicamp.inf321.models.screenobjects.widgets;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.Widget;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * @author Toni Isidoro (toni.isidoro@sofist.com.br)
 */
@AndroidFindAll({
        @AndroidBy(id = "br.com.sofist.ecommerce.ext:id/rvProducts"),
        @AndroidBy(id = "br.com.sofist.ecommerce.ext:id/my_recycler_view", priority = 1)
})
public class ProductListWidget extends Widget {
    protected ProductListWidget(WebElement element) {
        super(element);
    }

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/tvProductName")
    private List<MobileElement> products;

    public List<MobileElement> getProducts() {
        return products;
    }
}
