package br.com.unicamp.inf321.models.screenobjects;

import br.com.unicamp.inf321.models.screenobjects.widgets.MenuWidget;
import br.com.unicamp.inf321.models.screenobjects.widgets.ProductListWidget;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * @author Toni Isidoro (toni.isidoro@sofist.com.br)
 */
public class HomeObjects {

    @AndroidFindBy(accessibility = "Navigate up")
    private MobileElement openMenu;

    private MenuWidget menuWidget;

    private ProductListWidget productListWidget;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/search_src_text")
    private MobileElement editTextSearch;

    @AndroidFindBy(id = "br.com.sofist.ecommerce.ext:id/search_button")
    private MobileElement buttonSearch;

    public MobileElement getButtonSearch() {
        return buttonSearch;
    }

    public MenuWidget getMenuWidget() {
        return menuWidget;
    }

    public ProductListWidget getProductListWidget() {
        return productListWidget;
    }

    public MobileElement getOpenMenu() {
        return openMenu;
    }

    public MobileElement getEditTextSearch() {
        return editTextSearch;
    }
}
