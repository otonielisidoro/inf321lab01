package br.com.unicamp.inf321.helper;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.Widget;
import org.graphwalker.core.machine.ExecutionContext;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @author Toni Isidoro (toni.isidoro@sofist.com.br)
 */
public class TestHelpers extends ExecutionContext {
    public static final int IMPLICITLY_WAIT_TIME_OUT = 20;
    private AndroidDriver<MobileElement> driver;

    public TestHelpers(AndroidDriver<MobileElement> driver) {
        this.driver = driver;
    }

    public void waitForElementToBeClickable(MobileElement element) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public String getElementText(MobileElement element) {
        if (element != null && element.isDisplayed())
            return element.getText();
        else
            return "";
    }

    public void waitForElementToBeDisplayed(MobileElement element) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        wait.until(elementToBeDisplayed(element));
    }

    public void waitForElementToBeDisplayed(MobileElement element, Integer timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(elementToBeDisplayed(element));
    }

    public void waitForWidgetToBeDisplayed(Widget element) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        wait.until(widgetToBeDisplayed(element));
    }

    public void waitForWidgetToBeDisplayed(Widget element, Integer timeout) {
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        wait.until(widgetToBeDisplayed(element));
    }

    public static ExpectedCondition<Boolean> elementToBeDisplayed(final WebElement element) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return element.isDisplayed();
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return String.format("element %s to be displayed", element);
            }
        };
    }

    public void safeHideKeyboard() {
        if (((AndroidDriver) driver).isKeyboardShown()) {
            driver.hideKeyboard();
        }
    }

    public static ExpectedCondition<Boolean> widgetToBeDisplayed(final Widget element) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return element.getWrappedElement().isDisplayed();
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            public String toString() {
                return String.format("element %s to be displayed", element);
            }
        };
    }

    public void waitForElementToDisappear(String id) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(id)));
    }

    public MobileElement waitForElement(MobileElement arg) {
        waitForElementToBeDisplayed(arg);
        MobileElement el = arg;
        return el;
    }

    public Boolean waitForElementWithText(MobileElement element, String text) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        try {
            return wait.until(ExpectedConditions.textToBePresentInElement(element, text));
        } catch (Exception e) {
            return false;
        }
    }

    public boolean waitForToastTextToBePresent(String text) {
        WebDriverWait wait = new WebDriverWait(driver, IMPLICITLY_WAIT_TIME_OUT);
        return wait.until(toastTextToBePresent(text));
    }

    public static ExpectedCondition<Boolean> toastTextToBePresent(final String text) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement toastView = driver.findElement(By.xpath("//android.widget.Toast[1]"));
                    String toastText = toastView.getAttribute("name");
                    System.out.println(toastText);
                    return toastText.toLowerCase().contains(text.toLowerCase());
                } catch (StaleElementReferenceException var3) {
                    return null;
                }
            }

            public String toString() {
                return String.format("text ('%s') to be present", text);
            }
        };
    }

    public void swipeRightUntilElementIsPresentById(String id) {
        do {
            swipeRight();
        } while (!isElementPresent(By.id(id)));
    }

    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isElementPresentWithinTimeout(By by, Integer timeoutInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(by));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void swipeLeftUntilTextExists(String expected) {
        do {
            swipeLeft();
        } while (!driver.getPageSource().contains(expected));
    }

    public void swipeRight() {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.9);
        int endx = (int) (size.width * 0.20);
        int starty = size.height / 2;
        new TouchAction(driver).press(startx, starty)
                .waitAction(Duration.ofSeconds(2))
                .moveTo(endx, starty).release().perform();
    }

    public void swipeLeft() {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.8);
        int endx = (int) (size.width * 0.20);
        int starty = size.height / 2;
        new TouchAction(driver).press(startx, starty)
                .waitAction(Duration.ofSeconds(3000))
                .moveTo(endx, starty).release();
    }

    public void swipeVerticalUntilElementIsDisplayed(MobileElement element, boolean topToBottom, boolean swipeFirst) {
        WebDriverWait wait = new WebDriverWait(driver, 180);
        wait.until((ExpectedCondition<Boolean>) input -> {
            Boolean found = false;
            if (swipeFirst) {
                swipeVertical(topToBottom, 0);
            }
            try {
                if (element.isDisplayed()) {
                    found = true;
                } else {
                    swipeVertical(topToBottom, 0);
                }
            } catch (Exception e) {
                swipeVertical(topToBottom, 0);
            }
            return found;
        });
    }

    public void swipeVerticalUntilElementIsDisplayed(By element, boolean topToBottom, boolean swipeFirst) {
        LocalDateTime localDateTime = LocalDateTime.now();
        if (swipeFirst) {
            swipeVertical(topToBottom);
        }
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        while (driver.findElements(element).size() < 1) {
            long diff = ChronoUnit.SECONDS.between(localDateTime, LocalDateTime.now());
            if (diff > 180  ) {
                throw new ElementNotVisibleException("Element identified by " + element.toString() +
                        " was not found within the timeout of " + IMPLICITLY_WAIT_TIME_OUT + " seconds");
            }
            swipeVertical(topToBottom);
        }
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT_TIME_OUT, TimeUnit.SECONDS);
    }

    public void longPress(MobileElement element) {
        new TouchAction(driver).longPress(element)
                .perform();
    }

    public void swipeVertical(Boolean topToBottom) {
        swipeVertical(topToBottom, 1);
    }

    public void swipeVertical(Boolean topToBottom, Integer durationInSeconds) {
        //Get the size of screen.
        Dimension size = driver.manage().window().getSize();

        //Find swipe start and end point from screen's with and height.
        //Find starty point which is at bottom side of screen.
        int starty = (int) (size.height * 0.80);
        //Find endy point which is at top side of screen.
        int endy = (int) (size.height * 0.20);
        //Find horizontal point where you wants to swipe. It is in middle of screen width.
        int startx = size.width / 2;

        if (topToBottom) {
            new TouchAction(driver).press(startx, endy)
                    .waitAction(Duration.ofSeconds(durationInSeconds, 2))
                    .moveTo(startx, starty).release().perform();

        } else {
            new TouchAction(driver).press(startx, starty)
                    .waitAction(Duration.ofSeconds(durationInSeconds, 2))
                    .moveTo(startx, endy).release().perform();
        }
    }


    public void clickBackButton() {
        driver.navigate().back(); //Closes keyboard
        //driver.navigate().back(); //Comes out of edit mode
    }

    public AndroidDriver<MobileElement> getDriver() {
        return driver;
    }
}
